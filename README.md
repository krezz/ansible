# Ansible docker image with Kubernetes pack

[![pipeline status](https://gitlab.com/krezz/ansible/badges/main/pipeline.svg)](#)
[![python](https://gitlab.com/krezz/ansible/-/jobs/artifacts/main/raw/python.svg?job=badges)](#)
[![ansible-core](https://gitlab.com/krezz/ansible/-/jobs/artifacts/main/raw/ansible-core.svg?job=badges)](#)
[![ansible-community](https://gitlab.com/krezz/ansible/-/jobs/artifacts/main/raw/ansible-community.svg?job=badges)](#)
[![kubectl](https://gitlab.com/krezz/ansible/-/jobs/artifacts/main/raw/kubectl.svg?job=badges)](#)
[![helm](https://gitlab.com/krezz/ansible/-/jobs/artifacts/main/raw/helm.svg?job=badges)](#)
