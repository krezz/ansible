ARG PYTHON_VERSION="3.9"
FROM python:${PYTHON_VERSION}-slim-bullseye as ansible-core-build

ARG HELM_VERSION="3.10.2"
ARG KUBECTL_VERSION="1.23.4"
ARG ANSIBLE_CORE_VERSION="2.13.6"
ARG DEBIAN_FRONTEND=noninteractive

COPY requirements.txt ./

RUN apt-get update &&\
    apt-get install -qqy --no-install-recommends build-essential postgresql-client-13 \
    libpq-dev apt-transport-https gnupg gnupg2 git curl jq sudo zip unzip wget lsb-release netcat dnsutils &&\
    pip install --upgrade --no-cache-dir -r requirements.txt pip ansible-core==${ANSIBLE_CORE_VERSION} &&\
    wget -nvc https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xz -C /tmp &&\
    mv -vf /tmp/*/helm /usr/local/bin &&\
    wget -nvc https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl -O /usr/local/bin/kubectl &&\
    chmod +x /usr/local/bin/kubectl &&\
    apt-get update --fix-missing &&\
    apt-get remove -qqy --purge build-essential &&\
    apt-get autoremove -qqy --purge &&\
    rm -rf /var/lib/apt/lists/* ./*.txt &&\
    ansible --version &&\
    kubectl version --short --client &&\
    helm version --short &&\
    helm plugin install https://github.com/databus23/helm-diff

ENTRYPOINT [ "/usr/bin/env" ]
CMD [ "ansible", "--version" ]

# Add Ansible Community collections to the build
FROM ansible-core-build as ansible-community-build

ARG ANSIBLE_VERSION="6.6.0"

RUN pip install --no-cache-dir ansible==${ANSIBLE_VERSION}
